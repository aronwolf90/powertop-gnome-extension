const { parse_csv } = imports['..'].parser.parse_csv
const GLib = imports.gi.GLib;

describe('parse_csv', () => {
  let originalTimeout;

  const csv = "Usage;Events/s;Category;Description;PW Estimate\n" +
              "100,0%;;Device;Display backlight;  12.0 W\n" +
              "1,5%; 48,3;Process;[PID 4313] /usr/bin/gnome-shell ; 30.1 mW\n" +
              ""

  it("returns correct json for csv", () => {
    const result = parse_csv(csv)

    expect(result).toEqual(
      [
        { 
          'Usage': '100,0%',
          'Events/s': '',
          'Category': 'Device',
          'Description': 'Display backlight',
          'PW Estimate': '12.0 W',
        },
        {
          'Usage': '1,5%',
          'Events/s': '48,3',
          'Category': 'Process',
          'Description': '[PID 4313] /usr/bin/gnome-shell',
          'PW Estimate': '30.1 mW'
        }
      ]
    )
  })
});

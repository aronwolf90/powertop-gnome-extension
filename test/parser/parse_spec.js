const { parse } = imports['..'].parser.parse
const { Entry } = imports['..'].models.entry
const { Status } = imports['..'].models.status

describe('parser.parse', () => {
  const powerTopResult =
    " *  *  *   Top 10 Power Consumers   *  *  *\n" +
    "Usage;Events/s;Category;Description;PW Estimate\n" +
    "100,0%;;Device;Display backlight;  12.0 W\n" +
    "1,5%; 48,3;Process;[PID 4313] /usr/bin/gnome-shell ; 30.1 mW\n" +
    "\n" +
    "____________________________________________________________________\n" +
    " *  *  *   Overview of Software Power Consumers   *  *  *           \n" +
    "                                                                    \n" +
    "Usage;Wakeups/s;GPU ops/s;Disk IO/s;GFX Wakeups/s;Category;Description;PW Estimate\n"       +
    " 99,2 ms/s;161,2;;;;Process;[PID 11134] /opt/google/chrome/chrome --type=renderer;  397 mW\n" +
    "\n" +
    "The system baseline power is estimated at:  13.5  W;                "

  it("returns correct models.status", () => {
    expect(parse(powerTopResult)).toEqual(
      new Status({
        entries: [
          new Entry({ title: 'Display backlight', powerUsage: '12.0 W' }),
          new Entry({ title: '[PID 4313] /usr/bin/gnome-shell', powerUsage: '30.1 mW' }),
        ],
        total: 'The system baseline power is estimated at:  13.5  W;                ',
      })
    )
  })
});

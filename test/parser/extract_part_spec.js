const { extract_part } = imports['..'].parser.extract_part

describe('extract_part', () => {
  let originalTimeout;

  const powertopResult =
    "____________________________________________________________________\n" +
    "                        P o w e r T O P                             \n" +
    "                                                                    \n" +
    "____________________________________________________________________\n" +
    " *  *  *   System Information   *  *  *                             \n" +
    "                                                                    \n" +
    "PowerTOP Version;2.14 ran at Sun Aug 21 10:59:46 2022               \n" +
    "                                                                    \n" +
    "Kernel Version;Linux version 5.14.0-1048-oem                        \n" +
    "System Name;Dell Inc.0DMPXV                                         \n" +
    "CPU Information;8 11th Gen Intel(R) Core(TM) i7-1185G7 @ 3.00GHz    \n" +
    "                                                                    \n" +
    "Target: 1 units/s;System:  1822.7 wakeup/s;CPU:  36.5% usage;GPU: 0 ops/s;GFX: 98.2 wakeups/s;VFS: 0 ops/s\n" +
    "                                                                    \n" +
    "*  *  *   Top 10 Power Consumers   *  *  *                          \n" +
    "Usage;Events/s;Category;Description;PW Estimate                     \n" +
    "100,0%;;Device;Display backlight;12.0 W                             \n" +
    "                                                                    \n" +
    "____________________________________________________________________\n" +
    " *  *  *   Overview of Software Power Consumers   *  *  *           \n" +
    "                                                                    \n" +
    "Usage;Wakeups/s;GPU ops/s;Disk IO/s;GFX Wakeups/s;Category;Description;PW Estimate\n"       +
    " 99,2 ms/s;161,2;;;;Process;[PID 11134] /opt/google/chrome/chrome --type=renderer;  397 mW" +
    "                                                                    \n" +
    "The system baseline power is estimated at:  13.5  W;                \n" +
    "____________________________________________________________________\n" +
    " *  *  *   Device Power Report   *  *  *                            \n" +
    "                                                                    \n" +
    "Usage;Device Name;PW Estimate                                       \n" +
    " 46,5%;Display backlight;  12.0 W"


  it("returns correct substring", () => {
    const result = extract_part(
      powertopResult,
      'Overview of Software Power Consumers'
    )
    
    expect(result).toEqual(
      "                                                                    \n" +
      "Usage;Wakeups/s;GPU ops/s;Disk IO/s;GFX Wakeups/s;Category;Description;PW Estimate\n"       +
      " 99,2 ms/s;161,2;;;;Process;[PID 11134] /opt/google/chrome/chrome --type=renderer;  397 mW" +
      "                                                                    \n" +
      "The system baseline power is estimated at:  13.5  W;                "
    )
  })
});

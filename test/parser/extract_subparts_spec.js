const { extract_subparts } = imports['..'].parser.extract_subparts

describe('extract_subpart', () => {
  const part =
    "                                                                                          \n" +
    "Usage;Wakeups/s;GPU ops/s;Disk IO/s;GFX Wakeups/s;Category;Description;PW Estimate        \n" +
    " 99,2 ms/s;161,2;;;;Process;[PID 11134] /opt/google/chrome/chrome --type=renderer;  397 mW\n" +
    "                                                                                          \n" +
    "The system baseline power is estimated at:  13.5  W;                                        "

  it("returns correct substring", () => {
    const result = extract_subparts(part)
    
    expect(result).toEqual([
      "Usage;Wakeups/s;GPU ops/s;Disk IO/s;GFX Wakeups/s;Category;Description;PW Estimate        \n" +
      " 99,2 ms/s;161,2;;;;Process;[PID 11134] /opt/google/chrome/chrome --type=renderer;  397 mW",

      "The system baseline power is estimated at:  13.5  W;                                        ",
    ])
  })
});

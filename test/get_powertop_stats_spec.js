const { get_powertop_stats } = imports['..'].get_powertop_stats
const GLib = imports.gi.GLib;
const { Entry } = imports['..'].models.entry

describe('get_powertop_stats', () => {
  let originalTimeout;

  beforeEach(() => {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
  });

  afterEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  })

  const csv =
    " *  *  *   Top 10 Power Consumers   *  *  *\n" +
    "Usage;Events/s;Category;Description;PW Estimate\n" +
    "100,0%;;Device;Display backlight;  12.0 W\n" +
    "1,5%; 48,3;Process;[PID 4313] /usr/bin/gnome-shell ; 30.1 mW\n" +
    "\n" +
    "____________________________________________________________________\n" +
    " *  *  *   Overview of Software Power Consumers   *  *  *           \n" +
    "                                                                    \n" +
    "Usage;Wakeups/s;GPU ops/s;Disk IO/s;GFX Wakeups/s;Category;Description;PW Estimate\n"       +
    " 99,2 ms/s;161,2;;;;Process;[PID 11134] /opt/google/chrome/chrome --type=renderer;  397 mW\n" +
    "\n" +
    "The system baseline power is estimated at:  13.5  W;                "

  it("returns power top stats", (done) => {
    const command = ['echo', csv]

    get_powertop_stats(status => {
      expect(status.entries).toEqual(jasmine.any(Array))
      expect(status.entries[0]).toEqual(jasmine.any(Entry))
      expect(status.entries[0]).toEqual(jasmine.any(Entry))
      expect(!!status.total).toEqual(true)
      done()
    }, command)
  })
});

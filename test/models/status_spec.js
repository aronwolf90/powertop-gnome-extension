const { Status } = imports['..'].models.status
const { Entry } = imports['..'].models.entry

describe('models.Status', () => {
  const entry = new Entry({})
  const status = new Status({ 
    entries: [entry],
    total: '1.45 W'
  })

  it('#title', () => {
    expect(status.entries).toEqual([entry])
  })

  it('#powerUsage', () => {
    expect(status.total).toEqual('1.45 W')
  })
});

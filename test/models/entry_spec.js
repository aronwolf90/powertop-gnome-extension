const { Entry } = imports['..'].models.entry

describe('models.Entry', () => {
  const entry = new Entry({ 
    title: 'Description',
    powerUsage: '1.45 W'
  })

  it('#title', () => {
    expect(entry.title).toEqual('Description')
  })

  it('#powerUsage', () => {
    expect(entry.powerUsage).toEqual('1.45 W')
  })
});

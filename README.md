# Powertop Gnome Extension (Experimental)
Powertop Gnome Extension - Add an indicator to the top panel where one can see the
processes that are consuming the most power.

## Installation
Installation via git is performed by cloning the repo into your local gnome-shell extensions directory
(usually ~/.local/share/gnome-shell/extensions/):
```bash
$ git clone git@gitlab.com:aronwolf90/powertop-gnome-extension.git <extensions-dir>/powertop@aronwolf90.com
```

After cloning the repo, the extension is practically installed yet disabled.
In order to enable it, run the following command:
```bash
$ gnome-extensions enable powertop@aronwolf90.com
```

## Contribution
Contributions to this project are welcome.

Please follow these guidelines when contributing:
* DO NOT open unsolicited MRs unless they are for small fixes (e.g. typos).
* If you have a feature idea, open an issue and discuss it there before implementing.
  DO NOT open a MR as a platform for discussion

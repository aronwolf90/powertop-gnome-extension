// UI specific components
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const { Indicator } = Me.imports.gui.indicator

let menu;

function init() {}

function enable() {
    menu = new Indicator();
    Main.panel.addToStatusArea('indicator', menu);
}

function disable() {
    menu.destroy();
}

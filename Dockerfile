FROM docker:20.10.16

WORKDIR /app
COPY install-jasmine-gjs.sh .

RUN apk add gjs sudo git meson bash imagemagick --update && \
  ./install-jasmine-gjs.sh

const Gio = imports.gi.Gio;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const { parse } = Me.imports.parser.parse

var get_powertop_stats = (callback, command = ['bash', `${Me.path}/generate_toppower_report.sh`]) => {
  let proc = Gio.Subprocess.new(
    command,
    Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE
  );

  proc.communicate_utf8_async(null, null, (proc, res) => {
    let [, stdout, stderr] = proc.communicate_utf8_finish(res);

    if (stderr) log(stderr)

    if (stdout) {
      callback(parse(stdout))
    }
  })
}

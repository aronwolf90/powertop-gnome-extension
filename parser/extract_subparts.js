var extract_subparts = (part) => {
  return part.replace(/^ *\n/, '').replace(/\n *$/, '').split(/\n *\n/).map(subpart => {
    return subpart.replace(/^\n/, '')
  })
}

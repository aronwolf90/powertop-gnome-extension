const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const { parse_csv } = Me.imports.parser.parse_csv
const { extract_part } = Me.imports.parser.extract_part
const { extract_subparts } = Me.imports.parser.extract_subparts
const { Entry } = Me.imports.models.entry
const { Status } = Me.imports.models.status

const top10PowerConsumers = (powerTopResult) => {
  const top10PowerConsumersCsv =
    extract_part(powerTopResult, 'Top 10 Power Consumers')
  
  return parse_csv(top10PowerConsumersCsv)
}

const totalPowerConsume = (powerTopResult) => {
  const overviewOfSoftwarePowerConsumersPart =
    extract_part(
      powerTopResult,
      'Overview of Software Power Consumers'
    )

  return extract_subparts(overviewOfSoftwarePowerConsumersPart)[1]
}

var parse = (powerTopResult) => {
  return new Status({
    total: totalPowerConsume(powerTopResult),
    entries: top10PowerConsumers(powerTopResult)
      .map(powerConsumer => {
        return new Entry({
          title: powerConsumer['Description'],
          powerUsage: powerConsumer['PW Estimate'],
        })
      }),
  })
}

var extract_part = (text, name) => {
  const lines = text.split('\n')

  let inPart = false

  return lines.filter(line => {
    if (line.includes(name)) {
      inPart = true
      return false
    } else if (line.match(/___________/)) {
      inPart = false
    }
    return inPart
  }).join('\n')
}

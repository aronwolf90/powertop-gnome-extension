var parse_csv = (csv) => {
  const lines = csv.split("\n").filter(line => line.trim() !== '')
  let jsonArray = []
  const header = lines.shift().split(';')
  return lines.map(line => {
    const json = {}
    const columns = line.split(';')
    for (let i=0; i < header.length; i++) {
      json[header[i].trim()] = columns[i].trim()
    }
    return json
  })
}

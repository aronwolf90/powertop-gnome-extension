#!/bin/bash

mkdir -p integration/tmp

POD=$(
  docker run \
    --rm \
    --privileged=true \
    -v "$PWD":/home/gnomeshell/.local/share/gnome-shell/extensions/powertop@aronwolf90.com \
    -v "$PWD/integration":/home/gnomeshell/.local/share/gnome-shell/extensions/integration@aronwolf90.com \
    -td \
    ghcr.io/schneegans/gnome-shell-pod-35
)
export POD

do_in_pod() {
  docker exec --user gnomeshell --workdir /home/gnomeshell "${POD}" set-env.sh "$@"
}
export -f do_in_pod

make_screenshot() {
  docker cp "${POD}:/opt/Xvfb_screen0" integration/tmp && \
         convert xwd:integration/tmp/Xvfb_screen0 integration/tmp/capture.jpg
}
export -f make_screenshot

check_match() {
  make_screenshot
  DIFF=$(
    compare -metric AE "$1" \
                       integration/tmp/capture.jpg \
                       integration/tmp/diff.jpg 2>&1
  ) || true
  if [[ $DIFF =~ e\+ ]]; then
    DIFF=100000000
  fi
  NUMBER_TRY=0

  while [[ $DIFF -gt 930 ]] && [[ $NUMBER_TRY -lt 20 ]]
  do
    sleep 1
    NUMBER_TRY=$((NUMBER_TRY + 1))
    make_screenshot
    DIFF=$(
      compare -metric AE "$1" \
                         integration/tmp/capture.jpg \
                         integration/tmp/diff.jpg 2>&1
    ) || true
    if [[ $DIFF =~ e\+ ]]; then
      DIFF=100000000
    fi
  done
  
  [[ $DIFF -lt 930 ]] || (echo "$DIFF" && exit 1)
}
export -f check_match

sleep 2

do_in_pod wait-user-bus.sh 

set -e
echo "Start GNOME Shell."
sleep 10
do_in_pod systemctl --user start "gnome-xsession@:99"

echo "Wait some time until GNOME Shell has been started"
check_match 'integration/images/gnome_shell_started.jpg'

do_in_pod gnome-extensions enable powertop@aronwolf90.com
do_in_pod gnome-extensions enable integration@aronwolf90.com
check_match 'integration/images/extension_enabled.jpg'

do_in_pod xdotool key KP_Enter
do_in_pod xdotool key KP_Enter
check_match 'integration/images/without_workspace.jpg'

do_in_pod xdotool key Escape
do_in_pod xdotool key Escape
check_match "integration/images/initial.jpg"

echo "Stop container"
docker stop "${POD}"

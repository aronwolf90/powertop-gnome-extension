const Main = imports.ui.main;


function init() {}

function enable() {
  Main.panel.statusArea.aggregateMenu._power.hide();
}

function disable() {
  Main.panel.statusArea.aggregateMenu._power.show();
}

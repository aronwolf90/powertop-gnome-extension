#!/bin/sh
set -ex

cd /tmp
sudo rm -r jasmine-gjs || true
git clone https://github.com/ptomato/jasmine-gjs 
cd jasmine-gjs
meson _build
ninja -C _build
sudo ninja -C _build install

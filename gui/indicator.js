// Basic stuff
const {St, Clutter} = imports.gi;
const Lang = imports.lang;

// UI specific components
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;

const Mainloop = imports.mainloop;

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;

const GObject    = imports.gi.GObject;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const { get_powertop_stats } = Me.imports.get_powertop_stats

var Indicator = GObject.registerClass(
class Indicator extends PanelMenu.Button {
  _init () {
    super._init(0.0, 'Indicator');

    this.text = new St.Label({
      text: 'Powertop',
      y_align: Clutter.ActorAlign.CENTER,
    });
    this.actor.add_actor(this.text);

    this._refresh();
  }
  _refresh () {
    let that = this;

    get_powertop_stats(status => {
      that.menu._getMenuItems().forEach(function (item) {
        item.destroy();
      });
      that.menu.addMenuItem(
        new PopupMenu.PopupMenuItem(status.total)
      )
      that.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
      status.entries.forEach(function(entry) {
        let menuItem = new PopupMenu.PopupMenuItem(that._descriptionText(entry.title));
        let energyText = new St.Label({
          text: entry.powerUsage,
          y_align: Clutter.ActorAlign.CENTER,
          x_align: Clutter.ActorAlign.END,
          x_expand: true,
          y_expand: true,
        });
        menuItem.actor.add_child(energyText);
        that.menu.addMenuItem(menuItem);
      })
      Mainloop.timeout_add(60000, function () {
        that._refresh()
      })
    })
  }
  _descriptionText (text) {
	  const regex = /\/([^/]+)( |$)/
    if (text.match(regex)) {
      return text.match(regex)[1]
    } else {
      return text
    }
  }
});
